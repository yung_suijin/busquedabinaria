using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace binario
{
    internal class TablaArreglo
    {
        private string sql, conexion = "Server=localhost; Database=lineas; User Id=sa; Password=reallyStrongPwd123";
        private SqlConnection conexionSql;
        private SqlDataReader lector;
        
        public TablaArreglo()
        {
            try
            {
                conexionSql = new SqlConnection(conexion);
                conexionSql.Open();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public TablaLineas[] arreglo()
        {
            TablaLineas[] linea = null;

            sql = "SELECT [idLinea],[idEstacion],[estacion],[tipo],[estatus],[puntosInteres] FROM vias";
            
            using(var comando = new SqlCommand(sql, conexionSql))
            {
                using(lector = comando.ExecuteReader())
                {
                    var lista = new List<TablaLineas>();
                    
                    while(lector.Read())
                    {
                        lista.Add(new TablaLineas
                        {
                            idLinea = lector.GetInt32(0),
                            idEstacion = lector.GetInt32(1),
                            nombreEstacion = lector.GetString(2),
                            tipo = lector.GetInt32(3),
                            estatus = lector.GetInt32(4),
                            puntosInteres = lector.GetString(5)
                        });
                    }

                    linea = lista.ToArray();
                    return linea;
                }
            }
        }
    }
}