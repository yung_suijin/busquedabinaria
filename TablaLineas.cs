using System;

namespace binario
{
    internal class TablaLineas
    {
        public int idLinea { get; set; }
        public int idEstacion { get; set; }
        public string nombreEstacion { get; set; }
        public int tipo { get; set; }
        public int estatus { get; set; }
        public string puntosInteres { get; set; }
    }
}