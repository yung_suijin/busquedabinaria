using System;
using System.Collections.Generic;

namespace binario
{
    internal class Busqueda
    {
        public Dictionary<int, int> BuscarEstacion(TablaLineas[] arregloLineas, int n, string estacion)
        {
            int indexMenor = 0, indexMayor = n - 1, indexMedio, posicionEstacion;
            var estacionesEncontradas = new Dictionary<int, int>();
            var buscar = new Seleccion();

            //Array.Sort(arregloLineas, (x, y) => String.Compare(x.nombreEstacion, y.nombreEstacion));
            buscar.OrdenarSeleccion(arregloLineas, arregloLineas.Length);
            
            posicionEstacion = Array.FindIndex(arregloLineas, p => p.nombreEstacion == estacion);

            while (indexMenor <= indexMayor)
            {
                indexMedio = (indexMenor + indexMayor) / 2;

                if (posicionEstacion == indexMedio)
                {
                    int index = indexMedio;

                    while (estacion == arregloLineas[index].nombreEstacion)
                    {
                        if (arregloLineas[index].nombreEstacion == estacion)
                        {
                            estacionesEncontradas.Add(arregloLineas[index].idLinea, arregloLineas[index].idEstacion);
                            index++;
                        }
                    }
                    return estacionesEncontradas;
                }
                else if (posicionEstacion < indexMedio)
                {
                    indexMayor = indexMedio - 1;
                }
                else
                {
                    indexMenor = indexMedio + 1;
                }
            }

            return estacionesEncontradas;
        }
    }
}