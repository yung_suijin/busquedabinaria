﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace binario
{
    class Program
    {
        static void Main(string[] args)
        {
            string estacion;

            var tabla = new TablaArreglo();
            var busqueda = new Busqueda();
            var posicion = new Dictionary<int, int>();

            Console.Write("Ingrese una estacion: ");
            estacion = Console.ReadLine();

            TablaLineas[] arregloLineas = tabla.arreglo();
            posicion = busqueda.BuscarEstacion(arregloLineas, arregloLineas.Length, estacion);

            if(posicion.Count == 0)
                Console.WriteLine($"La estacion no fue encontrada.");
            else
                posicion.Select(p => $"La estacion se encuentra en la linea: {p.Key}, estacion: {p.Value}").ToList().ForEach(Console.WriteLine);
        }
    }
}