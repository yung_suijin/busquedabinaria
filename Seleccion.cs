namespace binario
{
    internal class Seleccion
    {
        public void OrdenarSeleccion(TablaLineas[] arregloLineas, int n)
        {
            for (int i = 0; i < n - 1; i++)
            {
                int minimo = i;
                for (int j = i + 1; j < n; j++)
                    if (arregloLineas[j].nombreEstacion.CompareTo(arregloLineas[minimo].nombreEstacion) < 0) minimo = j;

                TablaLineas temp = arregloLineas[i];
                arregloLineas[i] = arregloLineas[minimo];
                arregloLineas[minimo] = temp;
            }
        }
    }
}